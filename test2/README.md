# 班级：软件工程1班     学号：202010111509    姓名：刘佳杭

# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色boss，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有boss的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户ljh，给用户分配表空间，设置限额为50M，授予boss角色。
- 最后测试：用新用户ljh连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称boss，ljh，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色boss和用户ljh，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE boss;
GRANT connect,resource,CREATE VIEW TO boss;
CREATE USER ljh IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER ljh default TABLESPACE "USERS";
ALTER USER ljh QUOTA 50M ON users;
GRANT boss TO ljh;
```

> ![1](./images/1.png)
>
> 语句“ALTER USER ljh QUOTA 50M ON users;”是指授权ljh用户访问users表空间，空间限额是50M。

- 第2步：新用户ljh连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus ljh/123@pdborcl
SQL> show user;
USER is "ljh"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang

```

![2](./images/2.png)

- 第3步：用户hr连接到pdborcl，查询ljh授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM ljh.customers;
elect * from ljh.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM ljh.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

> ![3](./images/3.png)
>
> 测试一下用户hr,ljh之间的表的共享，只读共享和读写共享都测试一下。
> ljh用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

![4](./images/4.png)

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user ljh unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user ljh  account unlock;
```

![5](./images/5.png)

![6](./images/6.png)

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色boss和用户ljh。
> 新用户ljh使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

![7](./images/7.png)

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role boss;
drop user ljh cascade;
```

![8](./images/8.png)
