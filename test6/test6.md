### 软件工程1班  202010111509 刘佳杭



# 基于Oracle数据库的商品销售系统的设计

## 1.表及表空间设计方案

- 创建表空间

```sql
-- 表空间1：SYSTEM 表空间
CREATE TABLESPACE system_ts DATAFILE 'system01.dbf' SIZE 100M;

-- 表空间2：USERS 表空间
CREATE TABLESPACE users_ts DATAFILE 'users01.dbf' SIZE 100M;
```

![创建表空间](./images/创建表空间.png)

- 创建表

```sql
-- 用户表 (Users)
CREATE TABLE users (
  user_id         NUMBER PRIMARY KEY,
  username        VARCHAR2(50) NOT NULL,
  password        VARCHAR2(50) NOT NULL,
  email           VARCHAR2(100),
  phone_number    VARCHAR2(20),
  registration_date DATE
) TABLESPACE users_ts;

-- 商品表 (Products)
CREATE TABLE products (
  product_id           NUMBER PRIMARY KEY,
  product_name         VARCHAR2(100) NOT NULL,
  product_description  VARCHAR2(200),
  price                NUMBER(10, 2) NOT NULL,
  stock_quantity       NUMBER
) TABLESPACE users_ts;

-- 订单表 (Orders)
CREATE TABLE orders (
  order_id         NUMBER PRIMARY KEY,
  user_id          NUMBER,
  order_date       DATE,
  order_status     VARCHAR2(20),
  CONSTRAINT fk_orders_users FOREIGN KEY (user_id) REFERENCES users(user_id)
) TABLESPACE users_ts;

-- 订单详情表 (OrderDetails)
CREATE TABLE order_details (
  detail_id        NUMBER PRIMARY KEY,
  order_id         NUMBER,
  product_id       NUMBER,
  quantity         NUMBER,
  total_price      NUMBER(10, 2),
  CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id) REFERENCES orders(order_id),
  CONSTRAINT fk_order_details_products FOREIGN KEY (product_id) REFERENCES products(product_id)
) TABLESPACE users_ts;
```

![创建表](./images/创建表.png)

- 向users表中插入25000条随机数据

  ![user表数据插入](./images/user表数据插入.png)

- 向products表中插入25000条随机数据

  ![products表数据插入](./images/products表数据插入.png)

- 向orders表中插入25000条随机数据

  ![order表数据插入](./images/order表数据插入.png)

- 向orders_details表中插入25000条随机数据

  ![order_details表数据插入](./images/order_details表数据插入.png)

## 2.设计权限及用户分配方案

```sql
-- 创建用户 boss_user
CREATE USER "boss_user" IDENTIFIED BY password DEFAULT TABLESPACE users_ts QUOTA UNLIMITED ON users_ts;

-- 创建用户 boss_admin
CREATE USER "boss_admin" IDENTIFIED BY password DEFAULT TABLESPACE users_ts QUOTA UNLIMITED ON users_ts;
```

- 分配products表的查看、插入、更新权限给boss_user用户

  ```sql
  GRANT SELECT, INSERT, UPDATE ON products TO boss_user;
  GRANT SELECT, INSERT, UPDATE ON orders TO boss_user;
  GRANT SELECT, INSERT, UPDATE ON order_details TO boss_user;
  ```

- 分配所有的权限给boss_admin用户

  ```sql
  GRANT ALL PRIVILEGES TO boss_admin;
  ```

  

## 3.数据库中建立一个程序包

```sql
-- 创建程序包 SALES_PACKAGE
CREATE OR REPLACE PACKAGE sales_package AS
  -- 存储过程：创建订单
  PROCEDURE create_order(
    p_user_id    IN NUMBER,
    p_product_id IN NUMBER,
    p_quantity   IN NUMBER
  );

  -- 函数：计算订单总价
  FUNCTION calculate_order_total(
    p_order_id IN NUMBER
  ) RETURN NUMBER;

  -- 存储过程：取消订单
  PROCEDURE cancel_order(
    p_order_id IN NUMBER
  );

  -- 函数：获取用户的订单数量
  FUNCTION get_user_order_count(
    p_user_id IN NUMBER
  ) RETURN NUMBER;
END sales_package;
/

-- 实现存储过程和函数
CREATE OR REPLACE PACKAGE BODY sales_package AS
  -- 存储过程：创建订单
  PROCEDURE create_order(
    p_user_id    IN NUMBER,
    p_product_id IN NUMBER,
    p_quantity   IN NUMBER
  ) AS
    v_order_id  NUMBER;
  BEGIN
    -- 插入订单记录
    INSERT INTO orders (order_id, user_id, order_date, order_status)
    VALUES (order_seq.NEXTVAL, p_user_id, SYSDATE, 'PENDING')
    RETURNING order_id INTO v_order_id;

    -- 插入订单详情记录
    INSERT INTO order_details (detail_id, order_id, product_id, quantity)
    VALUES (detail_seq.NEXTVAL, v_order_id, p_product_id, p_quantity);

    COMMIT;
  END create_order;

  -- 函数：计算订单总价
  FUNCTION calculate_order_total(
    p_order_id IN NUMBER
  ) RETURN NUMBER AS
    v_total_price  NUMBER(10, 2);
  BEGIN
    -- 查询订单详情的总价
    SELECT SUM(quantity * price)
    INTO v_total_price
    FROM order_details od
    JOIN products p ON od.product_id = p.product_id
    WHERE od.order_id = p_order_id;

    RETURN v_total_price;
  END calculate_order_total;

  -- 存储过程：取消订单
  PROCEDURE cancel_order(
    p_order_id IN NUMBER
  ) AS
  BEGIN
    -- 更新订单状态为 CANCELLED
    UPDATE orders
    SET order_status = 'CANCELLED'
    WHERE order_id = p_order_id;

    COMMIT;
  END cancel_order;

  -- 函数：获取用户的订单数量
  FUNCTION get_user_order_count(
    p_user_id IN NUMBER
  ) RETURN NUMBER AS
    v_order_count  NUMBER;
  BEGIN
    -- 统计用户的订单数量
    SELECT COUNT(*)
    INTO v_order_count
    FROM orders
    WHERE user_id = p_user_id;

    RETURN v_order_count;
  END get_user_order_count;

END sales_package;
/
```

![创建程序包](./images/创建程序包.png)



## 4.数据库备份方案

- 创建备份脚本： 创建一个文本文件，命名为 `backup_script.rman`，包含以下内容：

  ```sql
  RUN {
    -- 设置备份保存路径
    CONFIGURE CHANNEL DEVICE TYPE DISK FORMAT '/path/to/backup/%U';
  
    -- 执行完全备份
    BACKUP DATABASE PLUS ARCHIVELOG;
  
    -- 执行增量备份
    BACKUP INCREMENTAL LEVEL 1 DATABASE PLUS ARCHIVELOG;
    
    -- 执行事务日志备份
    BACKUP ARCHIVELOG ALL DELETE INPUT;
  
    -- 验证备份文件的完整性
    BACKUP VALIDATE DATABASE ARCHIVELOG ALL;
  
    -- 释放备份设置
    RELEASE CHANNEL;
  }
  ```

- 执行备份脚本

  ```bash
  rman target / cmdfile='backup_script.rman'
  ```

  ![恢复管理](./images/恢复管理.png)